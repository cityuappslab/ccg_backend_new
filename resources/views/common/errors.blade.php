@if (count($errors) > 0)
    <!-- Form Error List -->
    <div class="alert alert-danger">
        <strong>Whoops! Something went wrong!</strong>

        <br><br>

        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


@if(Session::has('error_term'))
    <div class="alert alert-danger alert-flash">
        <ul>
            <li>{{ Session::get('error_term') }}</li>
        </ul>
    </div>

@endif
