@extends('layouts.app')

@section('content')

@section('maintainScroll')
    (function($){
    window.onbeforeunload = function(e){    
    window.name += ' [' + $(window).scrollTop().toString() + '[' + $(window).scrollLeft().toString();
    };
    $.maintainscroll = function() {
    if(window.name.indexOf('[') > 0)
    {
    var parts = window.name.split('['); 
    window.name = $.trim(parts[0]);
    window.scrollTo(parseInt(parts[parts.length - 1]), parseInt(parts[parts.length - 2]));
    }   
    };  
    $.maintainscroll();
    })(jQuery);
@endsection
    <div class="container">

        <!-- Display Validation Errors -->
        @if($completed==false)
            <div class="alert alert-danger">
                <strong>Some information of characters is not completed such as character,picture,audio</strong>
            </div>
        @endif
        @include('common.errors')
        <div class="col-sm-offset-1 col-sm-10">

            <div class="panel panel-default">
                <div class="panel-heading">
                    New Character Set
                </div>

                <div class="panel-body">

                <!-- New characterSet Form -->
                    <form action="{{url('manageCharacter/new-character')}}" method="POST" class="form-horizontal"
                          enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <input type="hidden" name="characterSet_id" value="{{ $characterSet_id}}">
                        <!-- character Name -->
                        <div class="form-group">
                            <label for="character-name" class="col-sm-3 control-label">Character</label>

                            <div class="col-sm-6">
                                <input type="text" name="oneCharacter" id="oneCharacter" class="form-control"
                                       value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="character-img" class="col-sm-3 control-label">Character Image</label>

                            <div class="col-sm-6">
                                <label for="exampleInputFile">File input</label>
                                <input type="file" id="exampleInputFile" class="form-control" name='image'>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="character-img" class="col-sm-3 control-label">Character Soundtrack</label>

                            <div class="col-sm-6">
                                <label for="exampleInputFile">File input</label>
                                <input type="file" id="exampleInputFile" class="form-control" name='audio'>
                            </div>
                        </div>


                    <!-- Add character Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-save"></i>Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>




            @foreach($characters as $character)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{'No '.$index++.' Character : '.$character->oneCharacter}}
                    </div>

                    <div class="panel-body">
                        <!-- Display Validation Errors -->
                        <!-- New Character Form -->
                        <form action="{{url('manageCharacter/character')}}" method="POST" class="form-horizontal"
                              enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="character_id" value="{{ $character->id }}">
                            <!-- character Name -->
                            <div class="form-group">
                                <label for="character-name" class="col-sm-3 control-label">Character</label>

                                <div class="col-sm-6">
                                    <input type="text" name="oneCharacter" id="oneCharacter" class="form-control"
                                           value="{{$character->oneCharacter}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="character-img" class="col-sm-3 control-label">Character Image</label>

                                <div class="col-sm-6">
                                    <label for="exampleInputFile">File input</label>
                                    <input type="file" id="exampleInputFile" class="form-control" name='image'>
                                </div>
                            </div>

                            @if($character->image_url!='null')
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-6">
                                        <img src="{{url($character->image_url)}}">
                                    </div>
                                </div>
                            @endif

                            <div class="form-group">
                                <label for="character-img" class="col-sm-3 control-label">Character Soundtrack</label>

                                <div class="col-sm-6">
                                    <label for="exampleInputFile">File input</label>
                                    <input type="file" id="exampleInputFile" class="form-control" name='audio'>
                                </div>
                            </div>

                            @if($character->audio_url!='null')
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-6">
                                        <audio controls>
                                            <source src="{{url($character->audio_url)}}" type="audio/mpeg">
                                            Your browser does not support the audio element.
                                        </audio>
                                    </div>
                                </div>
                        @endif

                        <!-- Add character Button -->
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-1">
                                    <button type="submit" name="submit" value="Edit" class="btn btn-default">
                                        <i class="fa fa-btn fa-save"></i>Save
                                    </button>
                                </div>
                                <div class="col-sm-3">
                                    <button type="submit" name="submit" value="Delete" class="btn btn-default">
                                        <i class="fa fa-btn fa-trash"></i>Delete
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            @endforeach

        </div>
    </div>
@endsection
