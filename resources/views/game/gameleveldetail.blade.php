@extends('layouts.app')

@section('content')

    <div class="container">


        <div class="col-sm-offset-1 col-sm-12">

            @include('common.errors')

            @if($game->isCompleted()==false)
                <div class="alert alert-danger">
                    <strong>Some information of game is not completed. Please select the character set and save</strong>
                </div>
            @endif


            

            
                {{--{{dd($gamelevels[$i*5])}}--}}
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{$game->gameName}} - Level {{($range[0]).'-'.(($range[0])+4)}}
                    </div>

                    <div class="panel-body">
                        <!-- Display Validation Errors -->

                        <!-- Edit Game Level Form -->
                        <form action="{{url('manageGameLevel/game-level-detail')}}" method="POST"
                              class="form-horizontal">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="game_id" value="{{$game->id}}">
                            <input type="hidden" name="block" value="{{$block}}">
                            <input type="hidden" name="index" value="{{0}}">
                            


                            <div class="form-group">
                                <label for="game-description" class="col-sm-3 control-label">
                                    Number of Col
                                </label>

                                <div class="col-sm-8">

                                    <select name="numOfCol" class="form-control">

                                        @foreach($numOfCols as $numOfCol)

                                            @if($gamelevels[0]['numOfCol']==$numOfCol)
                                                <option value={{$numOfCol}} selected>{{$numOfCol}}</option>
                                            @else
                                                <option value={{$numOfCol}}>{{$numOfCol}}</option>
                                            @endif
                                        @endforeach

                                    </select>

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="game-description" class="col-sm-3 control-label">
                                    Number of Row
                                </label>

                                <div class="col-sm-8">

                                    <select name="numOfRow" class="form-control">

                                        @foreach($numOfRows as $numOfRow)

                                            @if($gamelevels[0]['numOfRow']==$numOfRow)
                                                <option value={{$numOfRow}}
                                                        selected>{{$numOfRow}}</option>
                                            @else
                                                <option value={{$numOfRow}}>{{$numOfRow}}</option>
                                            @endif
                                        @endforeach

                                    </select>

                                </div>
                                
                            </div>

                            


                            <!-- Edit Game Level Button -->
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-save"></i>Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            

        </div>


    </div>




@endsection
