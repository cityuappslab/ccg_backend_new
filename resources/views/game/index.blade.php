@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="col-sm-offset-1 col-sm-10">

			<!-- Current games -->
			@if (count($games) > 0)
				<div class="panel panel-default">
					<div class="panel-heading">
						Current Game
					</div>

					<div class="panel-body">
						<table class="table table-striped game-table">
							<thead>
								<th>Game</th>
								<th>&nbsp;</th>
							</thead>
							<tbody>
								@foreach ($games as $game)
									<tr>
										<td class="table-text"><div>{{ $game->gameName }}</div></td>

										<!-- gamelevel Button -->
										<td>
											<form action="{{url('manageGameLevel/game-level') }}" method="GET">
												<input type="hidden" name="id" value="{{$game->id }}">
												{{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
												<button type="submit" id="{{ $game->id }}" class="btn btn-danger">
													<i class="fa fa-btn fa-edit"></i>Edit
												</button>
											</form>
										</td>

										<!-- game Delete Button -->
										<!-- <td>
											<form action="{{url('manageGame/delete-game')}}" method="POST">
												<input type="hidden" name="id" value="{{$game->id }}">
												{{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
												<button type="submit" id="{{ $game->id }}" class="btn btn-danger">
													<i class="fa fa-btn fa-trash"></i>Delete
												</button>
											</form>
										</td> -->
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			@endif
		</div>
	</div>
@endsection
