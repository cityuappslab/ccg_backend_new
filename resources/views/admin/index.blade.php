@extends('layouts.app')

@section('content')
    <div class="col-sm-offset-2 col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading ">
                Dashboard
            </div>

            <div class="panel-body">
                <a href="{{url('manageGame/game')}}">
                    <button type="button" class="btn btn-primary btn-lg btn-block">Manage Game</button>
                </a>
                <br>
                <a href="{{url('manageCharacterSet/character-set')}}">
                    <button type="button" class="btn btn-primary btn-lg btn-block">Manage Character Set</button>
                </a>
                <br>
                <a href="{{url('records')}}">
                    <button type="button" class="btn btn-primary btn-lg btn-block">Read Statistics</button>
                </a>
                <br>


                <br>
            </div>
        </div>


    </div>
    <div class="container">
    </div>
@endsection

