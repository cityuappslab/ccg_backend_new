<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeMatrixColumnName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trialrecord', function($table)
        {
            $table->dropColumn('[1][1]');
            $table->dropColumn('[1][2]');
            $table->dropColumn('[1][3]');
            $table->dropColumn('[1][4]');
            $table->dropColumn('[1][5]');
            $table->dropColumn('[2][1]');
            $table->dropColumn('[2][2]');
            $table->dropColumn('[2][3]');
            $table->dropColumn('[2][4]');
            $table->dropColumn('[2][5]');
            $table->dropColumn('[3][1]');
            $table->dropColumn('[3][2]');
            $table->dropColumn('[3][3]');
            $table->dropColumn('[3][4]');
            $table->dropColumn('[3][5]');
            $table->dropColumn('[4][1]');
            $table->dropColumn('[4][2]');
            $table->dropColumn('[4][3]');
            $table->dropColumn('[4][4]');
            $table->dropColumn('[4][5]');
            $table->string('row1col1');
            $table->string('row1col2');
            $table->string('row1col3');
            $table->string('row1col4');
            $table->string('row1col5');
            $table->string('row2col1');
            $table->string('row2col2');
            $table->string('row2col3');
            $table->string('row2col4');
            $table->string('row2col5');
            $table->string('row3col1');
            $table->string('row3col2');
            $table->string('row3col3');
            $table->string('row3col4');
            $table->string('row3col5');
            $table->string('row4col1');
            $table->string('row4col2');
            $table->string('row4col3');
            $table->string('row4col4');
            $table->string('row4col5');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
