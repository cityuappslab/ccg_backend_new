<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAllMatrixColumnToTrialrecord extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trialrecord', function($table){
            $table->dropColumn('[0][0]');
            $table->string('[1][1]');
            $table->string('[1][2]');
            $table->string('[1][3]');
            $table->string('[1][4]');
            $table->string('[1][5]');
            $table->string('[2][1]');
            $table->string('[2][2]');
            $table->string('[2][3]');
            $table->string('[2][4]');
            $table->string('[2][5]');
            $table->string('[3][1]');
            $table->string('[3][2]');
            $table->string('[3][3]');
            $table->string('[3][4]');
            $table->string('[3][5]');
            $table->string('[4][1]');
            $table->string('[4][2]');
            $table->string('[4][3]');
            $table->string('[4][4]');
            $table->string('[4][5]');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
