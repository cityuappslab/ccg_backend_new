<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    //
    protected $table = 'game';

    public function gamelevels()
    {
        return $this->hasMany('App\GameLevel', 'game_id', 'id');
    }

    public function isCompleted()
    {

        $gamelevels = $this->gamelevels()->get();

        foreach ($gamelevels as $gamelevel) {

            if ($gamelevel->isCompleted() == false)
                return false;

        }

        return true;

    }


    public function getDetail()
    {
        $data['error'] = false;
        if (!$this->isCompleted()) {
            $data['error'] = true;
            $data['msg'] = 'Game detail is not finished Please fill it';

            return false;

        } else {
            $data['Game'] = $this;

            $gameLevels = $this->gamelevels()->get();
            $index = 0;
            foreach ($gameLevels as $gameLevel) {
                $data['GameLevel'][$index++] = $gameLevel->getDetail();
            }

            $list = array(1, 21, 41);

            for ($j = 0; $j < 3; $j++) {
                $characterSet = CharacterSet::where('id',$gameLevels[$list[$j]]->characterSet_id)->first();


                $index = 0;

                $characters = $characterSet->characters()->get();


                $data['characterSet'][$j]['characterSet_id']= $characterSet->id;
                foreach ($characters as $character) {
                    if ($character->isCompleted()){
                        $data['characterSet'][$j][$index++] = $character->getDetail();
                    }
                }




            }


        }

//        return json_encode($data,JSON_UNESCAPED_UNICODE);




        return $data;

    }

    public function gameRecords()
    {
        return $this->hasMany(GameRecord::class);
    }
    


}
