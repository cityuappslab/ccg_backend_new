<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameRecord extends Model
{
    //
    protected $table = 'gamerecord';

    public function game()
    {
        return $this->belongsTo(Game::class);
    }

    public function trialRecords()
    {
        return $this->hasMany(TrialRecord::class);
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
