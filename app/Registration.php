<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{	

    protected $fillable = [
    	'reg_code','is_used'
    ];
}
