<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameLevel extends Model
{
    //

    protected $table = 'gamelevel';


    protected $fillable = [
        'game_id', 'characterSet_id', 'level', 'duration','numOfRow','numOfCol', 'text_disappear_time','decrease_percentage',
    ];


    public function game()
    {
        return $this->belongsTo(Game::class);
    }


    public function isCompleted()
    {
        $characterSet=CharacterSet::find($this->characterSet_id);
        if ($characterSet==null||!$characterSet->isCompleted()){
            return false;
        }

        return true;

    }

    public function getDetail()
    {
        $data['gamelevel_id'] = $this->id;
        $data['level'] = $this->level;
        $data['duration'] = $this->duration;
        $data['characterSet_id'] = $this->characterSet_id;
        $data['numOfCol'] = $this->numOfCol;
        $data['numOfRow'] = $this->numOfRow;
        $data['text_disappear_time'] = $this->text_disappear_time;
        $data['decrease_percentage'] = $this->decrease_percentage;


        return $data;
    }

}
