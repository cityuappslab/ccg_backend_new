<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrialRecord extends Model
{
    //
    protected $table = 'trialrecord';

    protected $casts = [
        'firsttime' => 'boolean',
    ];

}
