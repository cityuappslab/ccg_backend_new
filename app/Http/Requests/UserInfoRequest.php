<?php

namespace App\Http\Requests;

class UserInfoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
//        $owner=Auth::user();
//        if($this->request->id->equalTo($owner->id))

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'self_into' => 'required|min:0',
//            'profile_photo' => 'required|mimes:png,jpeg,bmp'
        ];
    }
}
