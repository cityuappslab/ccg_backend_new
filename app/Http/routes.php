<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

use Illuminate\Support\Facades\Auth;

Route::group(['middleware' => ['web']], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');

    Route::get('/', function () {

        return redirect('admin');
    })->middleware('guest');

//    Route::get('/tasks', 'TaskController@index');
//    Route::post('/task', 'TaskController@store');
//    Route::delete('/task/{task}', 'TaskController@destroy');


    Route::get('/game', function () {
        $data['error'] = false;

        $games = App\Game::all();

        $index = 0;
        foreach ($games as $game) {
            if ($game->isCompleted()) {
                   $data['games'][$index++] = $game->getDetail();
            }
        }


        return response()->json($data);
    });

    Route::get('/records/user', 'ResultSet\DataSetController@getUserRecord');
    Route::get('/records/users', 'ResultSet\DataSetController@index');
    Route::get('/records/game', 'ResultSet\DataSetController@getGameRecord');
    Route::get('/records/{id}', 'ResultSet\DataSetController@getRecord');


    Route::get('/stage', 'ResultSet\RecordController@getGameStages');

    Route::resource('/records', 'ResultSet\RecordController');

    Route::resource('/poststage','ResultSet\StageController');

    Route::post('/logout', function () {
        Auth::logout();
        $data['error'] = false;
        $data['msg'] = 'Logout successful';
        return response()->json($data, 200);
    });


    Route::get('/exportgamerecord/{gamerecord_id}', 'ExportController@exportByGameRecord');

    Route::get('/exportgame/{id}', 'ExportController@exportByGame');

    Route::get('/exportuser/user','ExportController@exportByUser');

    Route::controllers([
        'userinfo' => 'UserInfoController',
        'admin' => 'AdminController',
        'manageGame' => 'Admin\GameController',
        'manageGameLevel' => 'Admin\GameLevelController',
        'manageCharacterSet' => 'Admin\CharacterSetController',
        'manageCharacter' => 'Admin\CharacterController',
        '/' => 'FileController'
    ]);


});









//testing only---------------------------------------------------------------------------------------------------

//Route::controllers([
//    'test'=>'TaskController'
//]);


//Route::group(['middleware' => 'web'], function () {
//    Route::auth();
//
//    Route::get('/home', 'HomeController@index');
//});
