<?php

namespace App\Http\Controllers\ResultSet;

use App\User;
use App\Game;
use App\GameRecord;
use App\TrialRecord;
use App\ShowStage;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RecordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->user = Auth::user();

        if ($this->user == null) {
            $data['error'] = true;
            $data['msg'] = 'Please login before request this page';
            return response()->json($data, 200);
        }

        $this->user_id = $this->user->id;


    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $games = Game::all();

        $count = $games->count();
        $games = $games->toArray();

        for ($i = 0; $i < $count; $i++) {
            $games[$i]['url'] = url("/records/game?id=") . $games[$i]['id'];
        }

        $data['games'] = $games;

        return view('record.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data['error'] = false;


        $validator = Validator::make($request->all(),
            [
                'game_id' => 'required|exists:game,id',
                'gamelevel_id' => 'required|exists:gamelevel,id',
                'win' => 'required',
                'startingTimeStamp' => '', //required
                'records .*.trial' => 'required | integer | min:1',
                'records .*.responseTime' => 'required | min:0.000001',
                'records .*.numOfRow' => 'max:1',//required | 
                'records .*.numOfCol' => 'max:1',//required | 
                'records .*.char' => 'required | max:1',
                'records .*.outcome' => 'required | integer | max:1',
                'records.*.firsttime' => 'required',
                'records.*.ans' =>    'required | max:1',      
                'records.*.ansrow' => 'required | max:1',
                'records.*.anscol' => 'required | max:1',
                'records.*.row1col1' => 'max:1',
                'records.*.row1col2' => 'max:1',
                'records.*.row1col3' => 'max:1',
                'records.*.row1col4' => 'max:1',
                'records.*.row1col5' => 'max:1',
                'records.*.row2col1' => 'max:1',
                'records.*.row2col2' => 'max:1',
                'records.*.row2col3' => 'max:1',
                'records.*.row2col4' => 'max:1',
                'records.*.row2col5' => 'max:1',
                'records.*.row3col1' => 'max:1',
                'records.*.row3col2' => 'max:1',
                'records.*.row3col3' => 'max:1',
                'records.*.row3col4' => 'max:1',
                'records.*.row3col5' => 'max:1',
                'records.*.row4col1' => 'max:1',
                'records.*.row4col2' => 'max:1',
                'records.*.row4col3' => 'max:1',
                'records.*.row4col4' => 'max:1',
                'records.*.row4col5' => 'max:1',
                'accuracy' => 'required',

            ]
        );

        if ($validator->fails()) {

            $data['error'] = true;
            $data['msg'] = $validator->errors()->all();
        } else {
            $game_id = $request->game_id;
            $gamelevel_id = $request->gamelevel_id;
            $records = $request->records;
            $win = $request->win;
            $accuracy = $request->accuracy;
            $startingTimeStamp = $request->startingTimeStamp;

            $gameRecord = new GameRecord();
            $gameRecord->user_id = $this->user_id;
            $gameRecord->game_id = $game_id;
            $gameRecord->gamelevel_id = $gamelevel_id;
            $gameRecord->win = $win;
            $gameRecord->accuracy = $accuracy;
            $gameRecord->startingTimeStamp = $startingTimeStamp;
            $gameRecord->save();

            $gamerecord_id = GameRecord::orderBy('created_at', 'desc')->first()->id;
            foreach ($records as $record) {
                $trialRecord = new TrialRecord();
                $trialRecord->gamerecord_id = $gamerecord_id;
                $trialRecord->trial = $record['trial'];
                $trialRecord->responseTime = $record['responseTime'];
                $trialRecord->numOfRow = $record['numOfRow'];
                $trialRecord->numOfCol = $record['numOfCol'];
                $trialRecord->char = $record['char'];
                $trialRecord->outcome = $record['outcome'];
                $trialRecord->firsttime = $record['firsttime'];
                $trialRecord->ans = $record['ans'];
                $trialRecord->ansrow = $record['ansrow'];
                $trialRecord->anscol =  $record['anscol'];
                $trialRecord->row1col1 = $record['row1col1']; 
                $trialRecord->row1col2 = $record['row1col2'];
                $trialRecord->row1col3 = $record['row1col3'];
                $trialRecord->row1col4 = $record['row1col4'];
                $trialRecord->row1col5 = $record['row1col5'];
                $trialRecord->row2col1 = $record['row2col1'];
                $trialRecord->row2col2 = $record['row2col2'];
                $trialRecord->row2col3 = $record['row2col3'];
                $trialRecord->row2col4 = $record['row2col4'];
                $trialRecord->row2col5 = $record['row2col5'];
                $trialRecord->row3col1 = $record['row3col1'];
                $trialRecord->row3col2 = $record['row3col2'];
                $trialRecord->row3col3 = $record['row3col3'];
                $trialRecord->row3col4 = $record['row3col4'];
                $trialRecord->row3col5 = $record['row3col5'];
                $trialRecord->row4col1 = $record['row4col1'];
                $trialRecord->row4col2 = $record['row4col2'];
                $trialRecord->row4col3 = $record['row4col3'];
                $trialRecord->row4col4 = $record['row4col4'];
                $trialRecord->row4col5 = $record['row4col5'];

                $trialRecord->save();

            }
        }
        return response()->json($data, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getGameStages()
    {

        if (!isset($this->user_id)) {
            $data['error'] = true;
            $data['msg'] = 'Please login before request this page';
            return response()->json($data, 200);
        }

        $data['error'] = false;

        $data['user_id'] = $this->user_id;

        $gameRecords = GameRecord::where('user_id', $this->user_id)->where('win', 1)->get();


        $gamesnumber = DB::table('game')->orderBy('id', 'desc')->select('id')->first();
        $firstgame = DB::table('game')->select('id')->first();
        $firstgameid = $firstgame->id;
        $lastgameid = $gamesnumber->id;

        $data['stage'] = 0;


        if (!$gameRecords->isEmpty() && $gameRecords->count() >= 60) {
            $gameID = [11, 12, 13, 14];

            $gameCompleted = null;

            for ($i = 0; $i < 4; $i++) {
                for ($j = 0; $j < 60; $j++) {
                    $gameCompleted[$i][$j] = false;
                }
            }

            // dd($gameRecord);

            $game1 = $gameRecords->where("game_id", "11")->unique("gamelevel_id")->count();
            $game2 = $gameRecords->where('game_id', "12")->unique('gamelevel_id')->count();
            $game3 = $gameRecords->where('game_id', "13")->unique('gamelevel_id')->count();
            $game4 = $gameRecords->where('game_id', "14")->unique('gamelevel_id')->count();



            if ($game1 < 20 || $game2 < 20 || $game3 < 20 || $game4 < 20) {
                $data['stage'] = 0;
            } elseif ($game1 < 40 || $game2 < 40 || $game3 < 40 || $game4 < 40) {
                $data['stage'] = 1;
            } elseif ($game1 < 60 || $game2 < 60 || $game3 < 60 || $game4 < 60) {
                $data['stage'] = 2;
            } else {
                $data['stage'] = 3;
            }


        }
        switch ($data['stage']) {
            case 0:
                $data['msg'] = 'You have not clear first 20 level';
                break;
            case 1:
                $data['msg'] = 'You have not clear first 40 level';
                break;
            case 2:
                $data['msg'] = 'You have not clear all level';
                break;
            case 3:
                $data['msg'] = 'You have clear all level';
                break;
        }


        $currentLevel = DB::table('users')->join('gamerecord','users.id','=','gamerecord.user_id')->join('gamelevel','gamerecord.gamelevel_id','=','gamelevel.id')->where('gamerecord.win','=','1')->where('users.id',$data['user_id'])->groupBy('gamerecord.game_id')->get(['gamerecord.user_id','gamerecord.game_id','gamelevel.level', DB::raw('MAX(gamelevel.level) as level')]);

        $index = 0;
        foreach ($currentLevel as $level) {
            $data['currentLevel'][$index++] = $level;
        }

        $showstage = ShowStage::select('user_id','stage','shown')->where('user_id',$data['user_id'])->get();

        $data['showstage'] = $showstage;



        return response()->json($data, 200);
    }


}
