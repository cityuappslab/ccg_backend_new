<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use League\Csv\Writer;
use App\User;
use App\GameRecord;
use App\TrialRecord;
use Illuminate\Support\Facades\DB;
use Excel;


class ExportController extends Controller
{



	public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('admin');
    }


    public function exportByGameRecord($gamerecord_id){	

        $records=TrialRecord::join('gamerecord','trialrecord.gamerecord_id','=','gamerecord.id')->join('game','gamerecord.game_id','=','game.id')->join('users','gamerecord.user_id','=','users.id')->join('gamelevel','gamerecord.gamelevel_id','=','gamelevel.id')->where('gamerecord_id', $gamerecord_id)->select('users.name','game.gameName','gamelevel.level','gamerecord.startingTimeStamp as start_time','gamerecord.created_at as end_time','gamelevel.characterSet_id','gamelevel.duration as Duration_(s)','gamelevel.text_disappear_time as Text_Disappear_Time_(s)','gamelevel.decrease_percentage as Decrease_Percentage_(%)','trialrecord.trial','trialrecord.responseTime as Response_Time_(ms)','trialrecord.numOfRow','trialrecord.numOfCol','trialrecord.char','trialrecord.outcome','trialrecord.ans as answer','trialrecord.ansrow as answer_row','trialrecord.anscol as answer_col','trialrecord.firsttime as First_Time_(1_for_true,_0_for_false)','trialrecord.row1col1'
                                  ,'trialrecord.row1col2'
                                  ,'trialrecord.row1col3'
                                  ,'trialrecord.row1col4'
                                  ,'trialrecord.row1col5'
                                  ,'trialrecord.row2col1'
                                  ,'trialrecord.row2col2'
                                  ,'trialrecord.row2col3'
                                  ,'trialrecord.row2col4'
                                  ,'trialrecord.row2col5'
                                  ,'trialrecord.row3col1'
                                  ,'trialrecord.row3col2'
                                  ,'trialrecord.row3col3'
                                  ,'trialrecord.row3col4'
                                  ,'trialrecord.row3col5'
                                  ,'trialrecord.row4col1'
                                  ,'trialrecord.row4col2'
                                  ,'trialrecord.row4col3'
                                  ,'trialrecord.row4col4'
                                  ,'trialrecord.row4col5')->get();

            Excel::create('game_record_by_user', function($excel) use ($records) {

                $excel->sheet('Sheet1', function($sheet) use ($records) {

                    $sheet->fromArray($records);

                });

            })->export('xls');
    }



    public function exportByGame($id){ 


        // $records = GameRecord::join('trialrecord','gamerecord.id','=', 'trialrecord.gamerecord_id')->where('game_id', $id)->get();

        $records = GameRecord::join('trialrecord','gamerecord.id','=','trialrecord.gamerecord_id')->join('game','gamerecord.game_id','=','game.id')->join('users','gamerecord.user_id','=','users.id')->join('gamelevel','gamerecord.gamelevel_id','=','gamelevel.id')->where('gamerecord.game_id', $id)->select('users.name','game.gameName','gamelevel.level','gamerecord.startingTimeStamp as start_time','gamerecord.created_at as end_time','gamelevel.characterSet_id','gamelevel.duration as Duration_(s)','gamelevel.text_disappear_time as Text_Disappear_Time_(s)','gamelevel.decrease_percentage as Decrease_Percentage_(%)','trialrecord.trial','trialrecord.responseTime as Response_Time_(ms)','trialrecord.numOfRow','trialrecord.numOfCol','trialrecord.char','trialrecord.outcome','trialrecord.ans as answer','trialrecord.ansrow as answer_row','trialrecord.anscol as answer_col','trialrecord.firsttime as First_Time_(1_for_true,_0_for_false)','trialrecord.row1col1'
                                  ,'trialrecord.row1col2'
                                  ,'trialrecord.row1col3'
                                  ,'trialrecord.row1col4'
                                  ,'trialrecord.row1col5'
                                  ,'trialrecord.row2col1'
                                  ,'trialrecord.row2col2'
                                  ,'trialrecord.row2col3'
                                  ,'trialrecord.row2col4'
                                  ,'trialrecord.row2col5'
                                  ,'trialrecord.row3col1'
                                  ,'trialrecord.row3col2'
                                  ,'trialrecord.row3col3'
                                  ,'trialrecord.row3col4'
                                  ,'trialrecord.row3col5'
                                  ,'trialrecord.row4col1'
                                  ,'trialrecord.row4col2'
                                  ,'trialrecord.row4col3'
                                  ,'trialrecord.row4col4'
                                  ,'trialrecord.row4col5')->get();


        

    	Excel::create('game_record_by_game', function($excel) use ($records) {

    	    $excel->sheet('Sheet1', function($sheet) use ($records) {
    			
    			$sheet->fromArray($records);

    		});

		})->export('xls');  
    }


    public function exportByUser(Request $request){

        if (!isset($request->id)) {
            return redirect('records/users');
        }

        $id = $request->id;

        $records = GameRecord::join('trialrecord','gamerecord.id','=','trialrecord.gamerecord_id')->join('game','gamerecord.game_id','=','game.id')->join('users','gamerecord.user_id','=','users.id')->join('gamelevel','gamerecord.gamelevel_id','=','gamelevel.id')->where('gamerecord.user_id', $id)->select('users.name','game.gameName','gamelevel.level','gamerecord.startingTimeStamp as start_time','gamerecord.created_at as end_time','gamelevel.characterSet_id','gamelevel.duration as Duration_(s)','gamelevel.text_disappear_time as Text_Disappear_Time_(s)','gamelevel.decrease_percentage as Decrease_Percentage_(%)','trialrecord.trial','trialrecord.responseTime as Response_Time_(ms)','trialrecord.numOfRow','trialrecord.numOfCol','trialrecord.char','trialrecord.outcome','trialrecord.ans as answer','trialrecord.ansrow as answer_row','trialrecord.anscol as answer_col','trialrecord.firsttime as First_Time_(1_for_true,_0_for_false)','trialrecord.row1col1'
                                  ,'trialrecord.row1col2'
                                  ,'trialrecord.row1col3'
                                  ,'trialrecord.row1col4'
                                  ,'trialrecord.row1col5'
                                  ,'trialrecord.row2col1'
                                  ,'trialrecord.row2col2'
                                  ,'trialrecord.row2col3'
                                  ,'trialrecord.row2col4'
                                  ,'trialrecord.row2col5'
                                  ,'trialrecord.row3col1'
                                  ,'trialrecord.row3col2'
                                  ,'trialrecord.row3col3'
                                  ,'trialrecord.row3col4'
                                  ,'trialrecord.row3col5'
                                  ,'trialrecord.row4col1'
                                  ,'trialrecord.row4col2'
                                  ,'trialrecord.row4col3'
                                  ,'trialrecord.row4col4'
                                  ,'trialrecord.row4col5')->get();

        
        Excel::create('game_record_by_user', function($excel) use ($records) {

            $excel->sheet('Sheet1', function($sheet) use ($records) {
                
                $sheet->fromArray($records);

            });

        })->export('xls');  


    }
}


