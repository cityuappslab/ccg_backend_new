<?php

namespace App\Http\Controllers\Admin;

use Storage;
use App\Character;
use App\CharacterSet;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;


class CharacterController extends Controller
{

    private $imageFolderName = 'character';

    protected function storeAudio($file, $folderName)
    {
        //check fodler exist(under the public directory) or not if not create the fodler
        if (!file_exists($folderName)) {
            mkdir($folderName, 0777, true);
        }

        $fileName = str_random(40) . '.' . $file->getClientOriginalExtension();
        $folderName = $folderName . '/';

        $iterator = 0;
        do {
            $exists = File::exists($folderName . $fileName);
            $iterator++;
            if ($iterator > 20) {
                Log::debug('★Unable to upload the photo Please resolve it via the naming file method★');
                return false;
            }
        } while ($exists);

        $file->move($folderName, $fileName);
        return $folderName . $fileName;
    }

        protected function storeImage($file, $folderName)
    {
        //check fodler exist(under the public directory) or not if not create the fodler
        if (!file_exists($folderName)) {
            mkdir($folderName, 0777, true);
        }

        $fileName = str_random(40) . '.' . $file->getClientOriginalExtension();
        $folderName = $folderName . '/';

        $iterator = 0;
        do {
            $exists = File::exists($folderName . $fileName);
            $iterator++;
            if ($iterator > 20) {
                Log::debug('★Unable to upload the photo Please resolve it via the naming file method★');
                return false;
            }
        } while ($exists);

        $file->move($folderName, $fileName);
        return $folderName . $fileName;
    }


    //
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('admin');

    }


    public function getCharacter(Request $request)
    {
        $characterSet_id = $request->characterSet_id;

        $characters = Character::where('characterSet_id', $characterSet_id)->get();

        $index = 1;

        $characterSet = CharacterSet::find($characterSet_id);

        $completed = $characterSet->isCompleted();
//        dd($completed);

        return view('characterset.character',
            [
                'characterSet_id' => $characterSet_id,
                'completed' => $completed,
                'characters' => $characters,
                'index' => $index
            ]);

    }

    public function postCharacter(Request $request)
    {
        $character_id = $request->character_id;
        $oneCharacter = $request->oneCharacter;
        $submitType = $request->submit;
        // $image_url = '';
        $audio_url = '';

        $character = Character::find($character_id);

//        dd($request->file('audio'));


        $this->validate($request,
            [

                'character_id' => 'required|exists:character,id',
                'oneCharacter' => 'required|max:1',
                'audio' => 'mimes:mpga,mp3,3pg,wav,audio/mp3',
                'image' => 'mimes:jpeg,png,bmp,gif,svg'
            ]
        );

        $duplicateInTheSet = Character::where('oneCharacter', $oneCharacter)->where('characterSet_id', $character->characterSet_id)->where('id','!=', $character_id)->first();


        if ($duplicateInTheSet) {
            $error_term = 'This character \'' . $oneCharacter . '\' already exist in this Character Set';
            Session::flash('error_term', $error_term);
            return back();
        }

        // Check if the type is Edit or Delete
        if ($submitType == 'Edit') {

            if (Input::hasFile('audio')) {
                $audio_url = $this->storeAudio($file = $request->file('audio'), 'character/audio');
                $character->audio_url = $audio_url;
            }

            if(Input::hasFile('image')){
                $image_url = $this->storeImage($file = $request->file('image'), 'character/image');
                $character->image_url = $image_url;
            }


            $character->id = $character_id;
            $character->oneCharacter = $oneCharacter;

            $character->update();

        } else if ($submitType == 'Delete') {
            // $audio_url = $character->audio_url;
            // Storage::delete($audio_url);
            $character->delete();
        }

        return redirect()->back();
    }


    public function postNewCharacter(Request $request)
    {
        $oneCharacter = $request->oneCharacter;
        $characterSet_id = 0;
        $characterSet_id = $request->characterSet_id;



        $this->validate($request,
            [
                
                'characterSet_id' => 'required|exists:characterset,id',
                'oneCharacter' => 'required|max:1',
                'audio' => 'required|mimes:mpga,mp3,3pg,wav',
                
            ]
        );




        $duplicateInTheSet = Character::where('oneCharacter', $oneCharacter)->where('characterSet_id', $characterSet_id)->first();


        if ($duplicateInTheSet != null) {
            $error_term = 'This character \'' . $oneCharacter . '\' already exist in this Character Set';
            Session::flash('error_term', $error_term);
            return back();
        }


        if (Input::hasFile('audio')) { 

            $character = new Character();
            $character->oneCharacter = $oneCharacter;
            $character->characterSet_id = $characterSet_id;

        

            $audio_url = $this->storeAudio($file = $request->file('audio'), 'character/audio'); //public/character/audio
            $character->audio_url = $audio_url;

            $character->save();
        }

        return redirect()->back();

    }


}
