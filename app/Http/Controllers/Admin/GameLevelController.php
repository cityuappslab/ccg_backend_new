<?php

namespace App\Http\Controllers\Admin;

use App\CharacterSet;
use App\Game;
use App\GameLevel;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;


class GameLevelController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');

    }

    //
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getGameLevel(Request $request)
    {
        $game_id = $request->id;

        $game = Game::where('id', $game_id)->orderBy('created_at', 'desc')->first();


        if ($game->exists == false)
            return redirect('manageGame/game');

        $gamelevel[1] = GameLevel::where('game_id', $game_id)->where('level', 1)->first();
        // $difference[1] = $gamelevel[1]->duration - GameLevel::where('game_id', $game_id)->where('level', 2)->first()->duration;

        $duration[1] = $gamelevel[1]->duration;

        $percent[1] = $gamelevel[1]->decrease_percentage;

        $disappear[1] = $gamelevel[1]->text_disappear_time;



        $gamelevel[6] = GameLevel::where('game_id', $game_id)->where('level', 6)->first();
        // $difference[6] = $gamelevel[6]->duration - GameLevel::where('game_id', $game_id)->where('level', 7)->first()->duration;

        $duration[6] = $gamelevel[6]->duration;

        $percent[6] = $gamelevel[6]->decrease_percentage;

        $disappear[6] = $gamelevel[6]->text_disappear_time;



        $gamelevel[11] = GameLevel::where('game_id', $game_id)->where('level', 11)->first();
        // $difference[11] = $gamelevel[11]->duration - GameLevel::where('game_id', $game_id)->where('level', 12)->first()->duration;
        $duration[11] = $gamelevel[11]->duration;

        $percent[11] = $gamelevel[11]->decrease_percentage;

        $disappear[11] = $gamelevel[11]->text_disappear_time;



        $gamelevel[16] = GameLevel::where('game_id', $game_id)->where('level', 16)->first();
        // $difference[16] = $gamelevel[16]->duration - GameLevel::where('game_id', $game_id)->where('level', 17)->first()->duration;
        $duration[16] = $gamelevel[16]->duration;
        $percent[16] = $gamelevel[16]->decrease_percentage;

        $disappear[16] = $gamelevel[16]->text_disappear_time;




        $gamelevel[21] = GameLevel::where('game_id', $game_id)->where('level', 21)->first();
        // $difference[21] = $gamelevel[21]->duration - GameLevel::where('game_id', $game_id)->where('level', 22)->first()->duration;
        $duration[21] = $gamelevel[21]->duration;
        $percent[21] = $gamelevel[21]->decrease_percentage;

        $disappear[21] = $gamelevel[21]->text_disappear_time;



        $gamelevel[26] = GameLevel::where('game_id', $game_id)->where('level', 26)->first();
        // $difference[26] = $gamelevel[26]->duration - GameLevel::where('game_id', $game_id)->where('level', 27)->first()->duration;
        $duration[26] = $gamelevel[26]->duration;
        $percent[26] = $gamelevel[26]->decrease_percentage;

        $disappear[26] = $gamelevel[26]->text_disappear_time;



        $gamelevel[31] = GameLevel::where('game_id', $game_id)->where('level', 31)->first();
        // $difference[31] = $gamelevel[31]->duration - GameLevel::where('game_id', $game_id)->where('level', 32)->first()->duration;
        $duration[31] = $gamelevel[31]->duration;
        $percent[31] = $gamelevel[31]->decrease_percentage;

        $disappear[31] = $gamelevel[31]->text_disappear_time;




        $gamelevel[36] = GameLevel::where('game_id', $game_id)->where('level', 36)->first();
        // $difference[36] = $gamelevel[36]->duration - GameLevel::where('game_id', $game_id)->where('level', 37)->first()->duration;
        $duration[36] = $gamelevel[36]->duration;
        $percent[36] = $gamelevel[36]->decrease_percentage;

        $disappear[36] = $gamelevel[36]->text_disappear_time;



        $gamelevel[41] = GameLevel::where('game_id', $game_id)->where('level', 41)->first();
        // $difference[41] = $gamelevel[41]->duration - GameLevel::where('game_id', $game_id)->where('level', 42)->first()->duration;
        $duration[41] = $gamelevel[41]->duration;
        $percent[41] = $gamelevel[41]->decrease_percentage;

        $disappear[41] = $gamelevel[41]->text_disappear_time;




        $gamelevel[46] = GameLevel::where('game_id', $game_id)->where('level', 46)->first();
        // $difference[46] = $gamelevel[46]->duration - GameLevel::where('game_id', $game_id)->where('level', 47)->first()->duration;
        $duration[46] = $gamelevel[46]->duration;
        $percent[46] = $gamelevel[46]->decrease_percentage;

        $disappear[46] = $gamelevel[46]->text_disappear_time;



        $gamelevel[51] = GameLevel::where('game_id', $game_id)->where('level', 51)->first();
        // $difference[51] = $gamelevel[51]->duration - GameLevel::where('game_id', $game_id)->where('level', 52)->first()->duration;
        $duration[51] = $gamelevel[51]->duration;
        $percent[51] = $gamelevel[51]->decrease_percentage;

        $disappear[51] = $gamelevel[51]->text_disappear_time;



        $gamelevel[56] = GameLevel::where('game_id', $game_id)->where('level', 56)->first();
        // $difference[56] = $gamelevel[56]->duration - GameLevel::where('game_id', $game_id)->where('level', 57)->first()->duration;
        $duration[56] = $gamelevel[56]->duration;
        $percent[56] = $gamelevel[56]->decrease_percentage;

        $disappear[56] = $gamelevel[56]->text_disappear_time;



        $characterSets = CharacterSet::all();

        return view('game.gamelevel',
            [
                'game' => $game,
                'gamelevel' => $gamelevel,
                'characterSets' => $characterSets,
                // 'difference' => $difference,
                'disappear' => $disappear,
                'percent' => $percent,
                'duration' => $duration,
            ]);
    }


    public function postGameLevel(Request $request)
    {
        $this->validate($request,
            [
                //            'name_hk' => 'required|unique:tier1|regex:/^\p{Han}+$/u',
                'game_id' => 'required|exists:game,id',
                'level' => 'required|digits_between:1,60',
                'startTime' => 'required|digits_between:1,100',
                // 'disappearTime' => 'required|digits_between:1,100',
                // 'decreaseTime' => 'required|digits_between:1,100',
            ]
        );

        $game_id = $request->game_id;
        $level = $request->level;
        $startTime = $request->startTime;
        $disappearTime = $request->disappearTime;
        $decreaseTime = $request->decreaseTime;

        // if ($startTime - 19 * $decreaseTime <= 0) {
        //     return redirect()->back();
        // }

        $time = 0;
        for ($i = $level; $i <= $level + 4; $i++) {
            $gamelevel = GameLevel::where('game_id', $game_id)->where('level', $i)->first();

            $gamelevel->duration = $startTime;

            $gamelevel->decrease_percentage = $decreaseTime;


            if($time == 0){
                $gamelevel->text_disappear_time = $disappearTime;
            }else{
                // $gamelevel->text_disappear_time = $disappearTime- $disappearTime*(($decreaseTime+$time)/100);
                $gamelevel->text_disappear_time = $disappearTime*pow((100-$decreaseTime)/100, $time);
            }
            
            $gamelevel->update();
            $time++;
            
        }

        // return redirect()->back();
        return Redirect::to(URL::previous());
    }

    public function postGameLevelCharacter(Request $request){
        $this->validate($request,
            [
                //            'name_hk' => 'required|unique:tier1|regex:/^\p{Han}+$/u',
                'game_id' => 'required|exists:game,id',
                'level' => 'required|digits_between:1,60',
                'charaterset_id' => 'required|exists:characterset,id',
                // 'startTime' => 'required|digits_between:1,100',
                // 'disappearTime' => 'required|digits_between:1,100',
                // 'decreaseTime' => 'required|digits_between:1,100',
            ]
        );

        $game_id = $request->game_id;
        $level = $request->level;
        $charaterset_id = $request->charaterset_id;
        
        for($i = $level; $i <= $level + 19; $i++){
            $gamelevel = GameLevel::where('game_id', $game_id)->where('level', $i)->first();
            $gamelevel->characterSet_id = $charaterset_id;
            $gamelevel->update();
        }

        return Redirect::to(URL::previous());
    }




    public function getGameLevelDetail(Request $request)
    {


        $this->validate($request,
            [
                'game_id' => 'required|exists:game,id',
                'block' => 'required|integer|digits_between:1,3',
            ]
        );


        $data['game_id'] = $request->game_id;

        $data['game'] = Game::where('id', $data['game_id'])->orderBy('created_at', 'desc')->first();

        if ($data['game']->exists == false)
            return redirect('manageGame/game');

        $data['block'] = $request->block;
        $data['range'] = $range = [1, 5];

        switch ($data['block']) {
            case 1:
                $data['range'] = $range = [1, 5];
                break;
            case 2:
                $data['range'] = $range = [6, 10];
                break;
            case 3:
                $data['range'] = $range = [11, 15];
                break;
            case 4:
                $data['range'] = $range = [16, 20];
                break;
            case 5:
                $data['range'] = $range = [21, 25];
                break;
            case 6:
                $data['range'] = $range = [26, 30];
                break;
            case 7:
                $data['range'] = $range = [31, 35];
                break;
            case 8:
                $data['range'] = $range = [36, 40];
                break;
            case 9:
                $data['range'] = $range = [41, 45];
                break;
            case 10:
                $data['range'] = $range = [46, 50];
                break;
            case 11:
                $data['range'] = $range = [51, 55];
                break;
            case 12:
                $data['range'] = $range = [56, 60];
                break;
        }



        $data['gamelevels'] = GameLevel::where('game_id', $data['game_id'])->whereBetween('level', $range)->get()->toArray();


//        $data['characterSets'] = CharacterSet::all();

        $characterSets = CharacterSet::all();

        $characterSetIndex = 0;
        foreach ($characterSets as $characterSet) {
            if ($characterSet->isCompleted())
                $data['characterSets'][$characterSetIndex++] = $characterSet;
        }


        $data['numOfCols'] = [2, 3, 4, 5];
        $data['numOfRows'] = [2, 3, 4];


//        dd($data);

        return view('game.gameleveldetail', $data);
    }


    public function postGameLevelDetail(Request $request)
    {


        $this->validate($request,
            [
                'game_id' => 'required|exists:game,id',
                'block' => 'required|integer|digits_between:1,3',
                'index' => 'required|integer|digits_between:1,4',
                // 'numOfCol' => 'required|integer|digits_between:$numOfRow,5',
                'numOfRow' => 'required|integer|digits_between:1,4',
                'numOfCol' => 'required|integer|min:1,5|greater_than_field:numOfRow',

            ]
        );


        $game_id = $request->game_id;
        $index = $request->index;
        $block = $request->block;
        $numOfRow = $request->numOfRow;
        $numOfCol = $request->numOfCol;
        $range = [1, 20];


        $game = GameLevel::where('game_id', $game_id)->get();


        switch ($block) {
            case 1:
                $data['range'] = $range = [1, 5];
                break;
            case 2:
                $data['range'] = $range = [6, 10];
                break;
            case 3:
                $data['range'] = $range = [11, 15];
                break;
            case 4:
                $data['range'] = $range = [16, 20];
                break;
            case 5:
                $data['range'] = $range = [21, 25];
                break;
            case 6:
                $data['range'] = $range = [26, 30];
                break;
            case 7:
                $data['range'] = $range = [31, 35];
                break;
            case 8:
                $data['range'] = $range = [36, 40];
                break;
            case 9:
                $data['range'] = $range = [41, 45];
                break;
            case 10:
                $data['range'] = $range = [46, 50];
                break;
            case 11:
                $data['range'] = $range = [51, 55];
                break;
            case 12:
                $data['range'] = $range = [56, 60];
                break;
        }

        $level = [1, 2, 3, 4, 5];

        for ($i = 0; $i < 5; $i++) {
            $level[$i] = $range[0] + $index * 5 + $i;
        }


        $temp = null;

        for ($i = 0; $i < 5; $i++) {
            //update the record
            $game[$level[$i] - 1]->numOfCol = $numOfCol;
            $game[$level[$i] - 1]->numOfRow = $numOfRow;
            $game[$level[$i] - 1]->update();
            $temp[$i] = $game[$level[$i] - 1];
        }


        return redirect('/manageGameLevel/game-level?id='.$game_id);

    }


}
