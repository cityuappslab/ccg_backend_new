<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;


class FileController extends Controller
{
    //
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function getUserImageSmall($id)
    {
        $exists = Storage::disk('local')->exists('/userProfile/' . $id . '_small.png');
        if ($exists == 0) {
            $img = Image::make('../storage/app/userProfile/' . $id . '.png');
            Image::make($img)->encode('png', 75)->resize(30, 30)->save('../storage/app/userProfile/' .$id . '_small.png');
        }

        $img = Image::make('../storage/app/userProfile/' . $id . '_small.png');

        return $img->response('png');
    }

    public function getUserImageMedium($id)
    {

        $exists = Storage::disk('local')->exists('/userProfile/' . $id . '_medium.png');

        if ($exists == 0) {
            $img = Image::make('../storage/app/userProfile/' . $id . '.png');
            Image::make($img)->encode('png', 75)->resize(150, 150)->save('../storage/app/userProfile/'.$id . '_medium.png');
        }


        $img = Image::make('../storage/app/userProfile/' . $id . '_medium.png');

        return $img->response('png');
    }

    public function getUserImage($id)
    {
        $img = Image::make('../storage/app/userProfile/' . $id . '.png');

        return $img->response('png');
    }


}
