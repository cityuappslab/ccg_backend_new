<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CharacterSet extends Model
{
    //
    protected $table = 'characterset';

    public function characters()
    {
        return $this->hasMany('App\Character', 'characterSet_id', 'id');
    }

    public function isCompleted()
    {
        $characters = $this->characters()->get();

        $count=0;

//        dd($characters->toArray());

        foreach ($characters as $character) {

            if($character->isCompleted()==false){
//                dd($character);
                return false;
            }
            $count++;
        }

       
        if($count>=15){
            return true;
        }
        else{
            return false;
        }

    }
    
    public function addCharacter(){
        $character = new Character();
        $character->characterSet_id = $this->id;
        $character->oneCharacter= '.';
        $character->image_url = 'null';
        $character->audio_url = 'null';
        $character->save();

        $this->numOfCharacter=$this->numOfCharacter+1;
        $this->save();


        if($character)
            return $character;
        else
            return null;

    }
    
}
